<?php

namespace App\Http\Controllers;

use App\Http\Requests\CalculateRequest;
use App\Services\CalculateService;

class CalculateController extends Controller
{
    private CalculateService $calculateService;

    public function __construct(CalculateService $calculateService)
    {
        $this->calculateService = $calculateService;
    }

    public function index(CalculateRequest $request)
    {
        try {
            $this->calculateService->calc($request->validated());
        } catch (\Throwable $throwable) {
            return "Возникла ошибка " . $throwable->getMessage();
        }
    }
}
