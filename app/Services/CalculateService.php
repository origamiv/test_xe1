<?php

namespace App\Services;

use App\Models\Rate;

class CalculateService
{
    public function calc(array $params)
    {
        $lastRates = Rate::query()
            ->orderBy('id', 'desc')
            ->first();
        //dd($lastRates->rates);
        $from = $params['from'];
        $to = $params['to'];
        if ($to == 'USD') {
            return ($lastRates->rates->$from) * $params['amount'];
        } elseif ($from == 'USD') {
            return (1 / $lastRates->rates->$to) * $params['amount'];
        } else {
            return (($lastRates->rates->$to) / ($lastRates->rates->$from)) * $params['amount'];
        }
    }

}
