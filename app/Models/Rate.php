<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;

    protected $table = 'rates';

    protected $fillable = ['tick', 'rates'];

    protected function rates(): Attribute
    {
        return Attribute::make(
            get: fn(?string $value) => !empty($value) ? json_decode($value) : null,
        );
    }
}
