<?php

namespace App\Console\Commands;

use App\Models\Rate;
use Carbon\Carbon;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

class RatesParseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rates:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Start');
        try {
            $httpClient = new Client();
            $ratesSource = $httpClient
                ->get('https://www.xe.com/api/protected/midmarket-converter/', ['headers' => ['Authorization' => env('XE_AUTH')]])
                ->getBody()
                ->getContents();
            $ratesFromXe = json_decode($ratesSource, true);

            Rate::query()->updateOrCreate([
                'tick' => Carbon::createFromTimestamp($ratesFromXe['timestamp'] / 1000)->toDateTimeString(),
            ],
                [
                    'tick' => Carbon::createFromTimestamp($ratesFromXe['timestamp'] / 1000)->toDateTimeString(),
                    'rates' => json_encode($ratesFromXe['rates'])
                ]);
        } catch (\Throwable $throwable) {
            echo $throwable->getMessage();
        }


        $this->info('End');
        return Command::SUCCESS;
    }
}
